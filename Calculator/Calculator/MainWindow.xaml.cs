﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Calculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int first;
        int second;

        char op;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Button btn = (Button)sender;
            TxtValue.Text += btn.Content.ToString();
            second = Int32.Parse(TxtValue.Text);
        }

        private void Divide_Click(object sender, RoutedEventArgs e)
        {
            first = Int32.Parse(TxtValue.Text);
            op = '/';
            TxtValue.Clear();
        }

        private void Multiple_Click(object sender, RoutedEventArgs e)
        {
            first = Int32.Parse(TxtValue.Text);
            op = '*';
            TxtValue.Clear();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            first = Int32.Parse(TxtValue.Text);
            op = '+';
            TxtValue.Clear();
        }

        private void Sub_Click(object sender, RoutedEventArgs e)
        {
            first = Int32.Parse(TxtValue.Text);
            op = '-';
            TxtValue.Clear();
        }

        private void Equal_Click(object sender, RoutedEventArgs e)
        {
            int result = 0;
            if (op == '+')
            {
                result = first + second;
            }
            else if (op == '-')
            {
                result = first - second;
            }
            else if (op == '*')
            {
                result = first * second;
            }
            else if (op == '/')
            {
                result = first / second;
            }
            TxtValue.Text = result.ToString();
        }

        private void ClearEverything_Click(object sender, RoutedEventArgs e)
        {
            TxtValue.Clear();
        }
    }
}